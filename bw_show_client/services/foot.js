import http from "../utils/http"
//轮播图图片数据
export const indexImg = (params) => http.get("/indexImgs", params);
//首页 数据
export const Prod = (params) => http.get("/prod/moreBuyProdList", params);
//通知 数据
export const topNotice = (params) => http.get("/shop/notice/topNoticeList", params);
//标题数据  《每日上新 商城热卖 更多宝贝》
export const ProdTag = (params) => http.get("/prod/tag/prodTagList", params);
//每日上新 商城热卖 数据
export const _prodListById = (params) => http.get('/prod/tagProdList', { ...params });
// 更多宝贝 数据
export const prodListBaby = (params) => http.get('/prod/prodListByTagId', { ...params })
//通知的详情 
export const notice = (params) => http.get("/shop/notice/noticeList", params);
//通知的公告的详情
export const informDel = (id) => http.get(`/shop/notice/info/${id}`);
//标题 新品推荐 详情
export const getClassifyList = (params) => http.get("/prod/lastedProdPage", { ...params });
//标题 限时特惠 详情
export const getdiscountList = (params) => http.get("/prod/discountProdList", { ...params });
//标题 每日疯抢 详情
export const getmoreProdList = (params) => http.get("/prod/moreBuyProdList", { ...params })
//关于权限
export const SetUserInfo = (params) => http.put("/p/user/setUserInfo", params);
//热门搜索 数据 
export const getSearchByShopId = (params) => http.get("/search/hotSearchByShopId", { ...params });
//搜索 商品 详情
export const searchadd = (params) => http.get("/search/searchProdPage", { ...params });
//每日上新 详情
