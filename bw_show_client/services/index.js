import request from "../utils/http"

// 设置用户信息 用户授权之后拿到用户信息 然后保存用户信息
export const save_user_info = (data) => request.put("/p/user/setUserInfo",data)

// 获取token
export const _login = (data) => request.post("/login",data)

// 获取相关订单数
export const _load_num = () => request.get("/p/myOrder/orderCount")

// 获取收藏数
export const _show_num = () => request.get('/p/user/collection/count')

// 收藏商品列表
export const _like_list=()=>request.get('/p/user/collection/prods')

// 全部订单
export const _all_order = (data) => request.get("/p/myOrder/myOrder",data)

export const _prodListById = (params) => request.get('/prod/prodListByTagId', { ...params });

export const _getClassifyList = (params) => request.get("/prod/lastedProdPage", { ...params });

//取消订单
export const requestnoOrder=(params)=>request.put('/p/myOrder/cancel/'+params)