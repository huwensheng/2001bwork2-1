import request from "../../utils/http";
// 获取购物车数据
export const getCart_list = data => request.post('/p/shopCart/info');
// 添加修改购物车数据
export const upCart_list = param => request.post('/p/shopCart/changeItem', param);
// 获取购物车数量
export const getCart_num = param => request.get('/p/shopCart/prodCount');


// 删除所有购物车数据
export const del_All_Cart = param => request.deletes('/p/shopCart/deleteAll');
// 删除单个购物车数据
export const del_Cart = param => request.deletes('/p/shopCart/deleteItem', param);

// 结算，生成订单信息
export const create_order = params => request.post("/p/order/confirm", params);

// 提交订单，返回支付流水号
export const send_order = params => request.post("/p/order/submit", params);

// 根据订单号进行支付
export const order_pay = params => request.post("/p/order/pay", params);
