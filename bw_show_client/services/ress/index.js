import request from "../../utils/http";
// 获取收货地址数据
export const getRess_list = data => request.get('/p/address/list');
// 新增用户地址
export const addAddr = addrParam => request.post('/p/address/addAddr', addrParam);
// 设置默认地址
export const putAddr = addrId => request.put(`/p/address/defaultAddr/${addrId}`);
// 获取地址信息
export const getAddrInfo = addrId => request.get(`/p/address/addrInfo/${addrId}`);
// 修改地址
export const EditAddrInfo = addrParam => request.put('/p/address/updateAddr', addrParam);
// 删除地址
export const DelAddrInfo = addrId => request.delete(`/p/address/deleteAddr`, { id: addrId });
