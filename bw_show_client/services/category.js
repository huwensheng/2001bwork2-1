import http from "../utils/http"

// 分类左侧
export const category = (data) => http.get('/category/categoryInfo', data)
// 分类右侧
export const prodPage = (data) => http.get('/prod/pageProd', data)