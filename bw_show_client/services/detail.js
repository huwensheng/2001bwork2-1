import request from "../utils/http";

// 获取详情数据

// 根据商品id 获取到详情数据
export const _get_detail = id => request.get("/prod/prodInfo", { prodId: id });
// 添加购物车
export const _add_cart=(data)=>request.post("/p/shopCart/changeItem",data)
// 购物车
export const requestcarcount=()=>request.post("/p/shopCart/info")
// 收藏 
export const collect_list = (data) => request.post("/p/user/collection/addOrCancel", data)
// 判断是否收藏
export const collect_islist = (data) => request.get('/p/user/collection/isCollection', data)
// 评论数据
export const commentnum=(data)=>request.get("/prodComm/prodCommData",data)
// 立即购买 /p/order/confirm
export const _buy_Shop=(data)=>request.post("/p/order/confirm",data)
// 购物车商品的数量
export const _shop_num=()=>request.get("/p/shopCart/prodCount")
