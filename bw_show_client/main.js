import App from './App'
import goPage from "./utils/utils"

// #ifndef VUE3
import Vue from 'vue'
Vue.prototype.$goPage = goPage;

async function newCloud () {//定义云函数
    const n_cloud = new wx.cloud.Cloud({
        resourceEnv:"min-gucof",
        traceUser : true
    });
    //初始化
    await n_cloud.init();
    return n_cloud
}

Vue.prototype.$cloud = newCloud();

Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
    ...App
})
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
export function createApp() {
    const app = createSSRApp(App)
    return {
        app
    }
}
// #endif