import result from "./config";
import { formatUrl } from "./utils";
import Vue from "vue";
//  封装 get post delete put  这些方法
export const http = ({ timeout, baseUrl, errorHandle, requestHandle }) => {
    const request = async (url, method = "get", data = {}, header = {}) => {
        // https://bjwz.bwie.com/mall4j/api/getList?id=1&name="zhangsan"
        //  定义请求对象
        const requestObj = {
            originUrl: url,
            baseUrl,
            url: `${baseUrl}${url}`,
            method,
            data,
            header,
            timeout
        };
        const config = requestHandle && requestHandle(requestObj) || requestObj;
        //  请求对象是否是合法对象
        if (typeof config === 'object' && config.url && config.method) {
            try {
                const res = await uni.request(config);
                console.log(res, "--------------res-----------");
                if (Array.isArray(res) && res.length && res[1]) {
                    return res[1].data ? res[1].data : res[1];
                }
                //  返回错误状态
                return Promise.reject(res);
            } catch (error) {
                errorHandle && errorHandle(error);
                return Promise.reject(error);
            }
        }
    }

    return {
        post(url, data, header = {}) {
            return request(url, "post", data, header)
        },
        get(url, query = {}, header = {}) {
            //  格式化请求地址
            const formatendUrl = formatUrl(url, query);
            return request(formatendUrl, "get", {}, header);
        },
        put(url, data, header = {}) {
            return request(url, "PUT", data, header);
        },
        delete(url, params, header = {}) {
            return request(`${url}/${params.id}`, "DELETE", {}, header)
        },
        deletes(url, params, header = {}) {
            return request(`${url}`, "DELETE", params, header)
        }
    }
};


const request = http({
    timeout: 100000,
    baseUrl: result.baseUrl,
    requestHandle: config => {
        //  设置请求拦截
        //  设置authorization 设置token
        const headers = {
            "Authorization": 'bearer' + uni.getStorageSync("token")
        };
        //  判断请求地址是否在白名单中  在的话  删除对象中的authorization
        if (result.whiteList.includes(config.originUrl)) {
            delete headers["Authorization"];
        }
        return {
            ...config,
            header: headers
        }
    },
    errorHandle: error => { // 错误处理
        // 打印下错误码
        console.log(error)
    }
});


export default request;

// 挂载到vue的原型上
Vue.prototype.$request = request;
