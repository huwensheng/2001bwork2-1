export const formatUrl = (url, query) => {
    /*
        url:https://bjwz.bwie.com/mall4j/api/getList?name=zhangsan&id=1
        {
            name:"zhangsan",
            id:1
        }
    */ 
   return Object.keys(query).length ? (url + "?" + Object.keys(query).map(key => `${key}=${query[key]}`).join("&")) : url
}


export default function ({url = "", query = {}, redirect = false, tabbar = false}) {
    // 跳转页面是tabbar swicthTab
    // redirect redirectTo
    // 默认使用navigateTo
    url = formatUrl(url, query);
    const typeName = redirect ? "redirectTo" : tabbar ? "switchTo" : "navigateTo";
    // console.log(typeName);
    return new Promise((resolve, reject) => {
        uni[typeName]({
            url,
            success(res) {
                resolve(res)
            },
            fail(err) {
                reject(err)
            }
        })
    })
}